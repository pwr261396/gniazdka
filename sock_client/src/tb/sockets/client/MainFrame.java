package tb.sockets.client;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ConnectException;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.FieldPosition;
import java.text.Format;
import java.text.ParseException;
import java.text.ParsePosition;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import javax.swing.text.MaskFormatter;

//import gniazkda.SerwerGUI.Obsluga;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JFormattedTextField;
import java.awt.Color;

public class MainFrame extends JFrame {
	//
	private JPanel contentPane;
	private PrintWriter pisz;
	private BufferedReader czytaj;
	private Socket gniazdo;
	private String wiadomosc;
	private JFormattedTextField frmtdtxtfldIp;
	private JButton btnConnect;
	private OrderPane panel;
	private JLabel lblNotConnected;
	private JButton rozlacz;
	private JLabel lblPort;
	private boolean ruch = false;
	private JFormattedTextField frmtdtxtfldXxxx;
	private JLabel lblruch;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainFrame frame = new MainFrame();					
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MainFrame() {

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 650, 500);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		JLabel lblHost = new JLabel("Host:");
		lblHost.setBounds(10, 14, 26, 14);
		contentPane.add(lblHost);

		frmtdtxtfldIp = new JFormattedTextField();
		frmtdtxtfldIp.setBounds(43, 11, 90, 20);
		frmtdtxtfldIp.setText("127.0.0.1");
		contentPane.add(frmtdtxtfldIp);

		btnConnect = new JButton("Connect");
		btnConnect.setBounds(10, 70, 125, 23);
		contentPane.add(btnConnect);
		btnConnect.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					polacz();
					konfiguruj();
					Thread watek = new Thread(new Odbieraj());
					watek.start();
					btnConnect.setEnabled(false);
					rozlacz.setEnabled(true);
				} catch (IOException ioException) {
					ioException.printStackTrace();
				}
			}
		});

		rozlacz = new JButton("Disconnect");
		rozlacz.setBounds(10, 150, 125, 23);
		contentPane.add(rozlacz);
		rozlacz.setEnabled(false);
		rozlacz.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				btnConnect.setEnabled(true);
				rozlacz.setEnabled(false);
				zamknij();
				panel.ruchy=-1;
				for (int i = 0; i < 9; i++) {
					panel.btn[i].setText("");
					panel.btn[i].setEnabled(true);
					
				}
			}
		});
		frmtdtxtfldXxxx = new JFormattedTextField();
		frmtdtxtfldXxxx.setText("9000");
		frmtdtxtfldXxxx.setBounds(43, 39, 90, 20);
		contentPane.add(frmtdtxtfldXxxx);

		lblPort = new JLabel("Port:");
		lblPort.setBounds(10, 42, 26, 14);
		contentPane.add(lblPort);
		panel = new OrderPane();
		panel.setBounds(145, 14, 487, 448);
		contentPane.add(panel);
		for (int i = 0; i <= 8; i++) {
			final int liczba = i;
			String nrpola = Integer.toString(i);
			panel.btn[i].addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					pisz.println("X" + nrpola);
					panel.btn[liczba].setText("X");
					panel.btn[liczba].setEnabled(false);
					panel.stangry("X");
					System.out.println(panel.ruchy);
					mojruch();
				}
			});
		}

		lblNotConnected = new JLabel("Not Connected");
		lblNotConnected.setForeground(new Color(255, 255, 255));
		lblNotConnected.setBackground(new Color(128, 128, 128));
		lblNotConnected.setOpaque(true);
		lblNotConnected.setBounds(10, 104, 123, 23);
		contentPane.add(lblNotConnected);
		lblruch =new JLabel("RUCH");
		lblruch.setBounds(10, 180, 125, 20);
		contentPane.add(lblruch);

	}

	public class Odbieraj implements Runnable {

		@Override
		public void run() {
			try {
				while ((wiadomosc = czytaj.readLine()) != null) {
					graj(wiadomosc);					
				}
			} catch (IOException ex) {
				// ex.printStackTrace();
			}

		}

		private void graj(String wiadomosc) throws IOException {
			if(wiadomosc.equals("TY")) {
				//lblruch.setText("MOJ RUCH");
				ruch=false;
				mojruch();
			}else if(wiadomosc.equals("JA")) {
				//lblruch.setText("RUCH PRZECIWNIKA");
				ruch=true;
				mojruch();
			}
			
			wiadomosc = wiadomosc.substring(1);
			for (int i = 0; i <= 8; i++) {
				if (wiadomosc.equals(Integer.toString(i))) {
					panel.btn[i].setText("O");
					panel.btn[i].setEnabled(false);
				}

			}
			panel.stangry("O");
			mojruch();
		}
	}

	private void zamknij() {
		System.out.println("Zamykam polaczenie! ");
		try {
			pisz.close();
			czytaj.close();
			gniazdo.close();
			lblNotConnected.setText("Disonnected");
		} catch (IOException ioException) {
			ioException.printStackTrace();
		}
	}

	private void konfiguruj() throws IOException {
		InputStreamReader czyt = new InputStreamReader(gniazdo.getInputStream());
		czytaj = new BufferedReader(czyt);
		pisz = new PrintWriter(gniazdo.getOutputStream(), true);
		System.out.println("Strumienie gotowe");
	}

	private void polacz() throws IOException {
		try {
			String host = frmtdtxtfldIp.getText();
			int port = Integer.parseInt(frmtdtxtfldXxxx.getText());
			gniazdo = new Socket(host, port);
			System.out.println("Probuje polaczyc z serwerem");
			System.out.println("Polaczylem sie z : " + host);
			lblNotConnected.setText("Connected");
		} catch (ConnectException connectException) {
			System.out.println("Uwaga! SERWER OBECNIE NIEDOSTEPNY");
		} catch (Exception ex) {
			System.out.println("cos poszlo nie tak");
		}
	}
	
	private void mojruch() {
		if(ruch){
			lblruch.setText("MOJ RUCH");
		}
		else {
			lblruch.setText("RUCH PRZECIWNIKA");
		}
		ruch=!ruch;
	}

}
