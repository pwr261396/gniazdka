package tb.sockets.client;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;

public class OrderPane extends JPanel {
	JButton btn[] = new JButton[9];
	JOptionPane message;
	int ruchy;

	/**
	 * Create the panel.
	 */
	public OrderPane() {
		ruchy=-1;
		setBackground(new Color(255, 255, 240));
		setLayout(new GridLayout(3, 3));
		for (int i = 0; i <= 8; i++) {
			btn[i] = new JButton();
			add(btn[i]);
			btn[i].setFont(new Font("Tahoma", Font.BOLD, 84));
		}
	}

	public void stangry(String w) {
		ruchy++;
		if (btn[0].getText().equals(w) && btn[3].getText().equals(w) && btn[6].getText().equals(w)) {
			message.showMessageDialog(null, "Koniec gry");
			zablokuj();
		} else if (btn[1].getText().equals(w) && btn[4].getText().equals(w) && btn[7].getText().equals(w)) {
			message.showMessageDialog(null, "Koniec gry");
			zablokuj();
		} else if (btn[2].getText().equals(w) && btn[5].getText().equals(w) && btn[8].getText().equals(w)) {
			message.showMessageDialog(null, "Koniec gry");
			zablokuj();
		} else if (btn[0].getText().equals(w) && btn[1].getText().equals(w) && btn[2].getText().equals(w)) {
			message.showMessageDialog(null, "Koniec gry");
			zablokuj();
		} else if (btn[3].getText().equals(w) && btn[4].getText().equals(w) && btn[5].getText().equals(w)) {
			message.showMessageDialog(null, "Koniec gry");
			zablokuj();
		} else if (btn[6].getText().equals(w) && btn[7].getText().equals(w) && btn[8].getText().equals(w)) {
			message.showMessageDialog(null, "Koniec gry");
			zablokuj();
		} else if (btn[0].getText().equals(w) && btn[4].getText().equals(w) && btn[8].getText().equals(w)) {
			message.showMessageDialog(null, "Koniec gry");
			zablokuj();
		} else if (btn[2].getText().equals(w) && btn[4].getText().equals(w) && btn[6].getText().equals(w)) {
			message.showMessageDialog(null, "Koniec gry");
			zablokuj();
		}else if(ruchy==9) {
			message.showMessageDialog(null, "Remis");
			zablokuj();
			ruchy=-1;
		}
	}
	public void zablokuj() {
		for(int i=0;i<9;i++) {
			btn[i].setEnabled(false);
		}
	}


}
