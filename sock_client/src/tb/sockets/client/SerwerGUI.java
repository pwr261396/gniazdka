package tb.sockets.client;

import java.net.*;
import java.util.*;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import javax.swing.*;



public class SerwerGUI {
	
	
	private JTextArea text = new JTextArea(5, 10);
	public PrintWriter pisz;
	private BufferedReader czytaj;
	private Socket gniazdo;
	private ServerSocket serwer;
	private JButton btn;
	private JTextField klient;
	private JButton wyslij;
	
	
	
	public static void main(String[] args) {

		SerwerGUI serwer = new SerwerGUI();
		serwer.stworzsie();
		serwer.dzialaj();

	}

	private void dzialaj() {
		try {
			serwer = new ServerSocket(9000);

			while (true) {
				gniazdo = serwer.accept();
				pisz = new PrintWriter(gniazdo.getOutputStream(), true);
				Thread watek = new Thread(new Obsluga(gniazdo));
				watek.start();

			}
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			try {
				pisz.close();
				czytaj.close();
				gniazdo.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}

	}


	public class Obsluga implements Runnable {
		public Obsluga(Socket socket) {
			try {
				gniazdo = socket;
				// pisz = new PrintWriter(gniazdo.getOutputStream(),true);
				InputStreamReader czyt = new InputStreamReader(gniazdo.getInputStream());
				czytaj = new BufferedReader(czyt);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}

		@Override
		public void run() {
			String wiad;
			
			try {	
					while ((wiad = czytaj.readLine()) != null) {
						System.out.println("Serwer odczyt: " + wiad);
						text.append(wiad + "\n");				
					}
			} catch (IOException ex) {
				ex.printStackTrace();
				System.out.println("Utracono polaczenie");
			}
		}

	}

	public void stworzsie() {
		JFrame ramka = new JFrame("Serwer");
		JPanel panel = new JPanel();
		btn = new JButton("halo");
		wyslij = new JButton("klient");
		klient = new JTextField(20);
		panel.add(text);
		panel.add(btn);
		panel.add(wyslij);
		panel.add(klient);


		btn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				text.append("dzialam \n");

			}
		});
		wyslij.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					pisz.println(klient.getText());
				} catch (Exception ex) {
					ex.printStackTrace();
				}
				klient.setText("");
				klient.requestFocus();
			}
		});

		ramka.getContentPane().add(BorderLayout.CENTER, panel);
		ramka.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		ramka.setSize(400, 400);
		ramka.setVisible(true);
	}

}
